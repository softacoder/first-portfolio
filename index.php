<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Personal portfolio website">
    <meta name="author" content="Mithun Chakrabarty">

    <title>Creative - Start Bootstrap Theme</title>

    <link rel=icon href=favicon.ico>
    <!-- Bootstrap Core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>Menu</i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">Portfolio</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="page-scroll" href="#about">About</a>
                </li>
                <li>
                    <a class="page-scroll" href="#services">Services</a>
                </li>
                <li>
                    <a class="page-scroll" href="#portfolio">Portfolio</a>
                </li>
                <li>
                    <a class="page-scroll" href="#contact">Contact</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<header>
    <div class="header-content">
        <div class="profile-image">
            <img class="img-circle" width="120px" height="120px" src="img/mithun.jpg">
        </div>
        <div class="header-content-inner">
            <h1 id="homeHeading">Hello There</h1>
            <hr>
            <p>I'm Mithun Chakrabarty, front-end and back-end web developer</p>
            <a href="#about" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
        </div>
    </div>
</header>

<section class="bg-primary" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 class="section-heading">Overview</h2>
                <hr class="light">
                <p class="text-faded">I am a Web development expert with over 2 years experience in setup and
                    customization of WordPress, Phalcon PHP, Laravel, Core PHP, JavaScript, JQuery, MySQL and others.
                    Till date, I have
                    created over 10 websites, most of which required custom designed plugins, extensions, and themes. My
                    aim has always been to produce websites that look beautiful and professional, are highly functional,
                    load quickly, and are Search Engine optimized. If this is what you want, then get in touch with
                    me.</p>
                <a href="#services" class="page-scroll btn btn-default btn-xl sr-button">My services!</a>
            </div>
        </div>
    </div>
</section>

<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">My service for you!</h2>
                <hr class="primary">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box">
                    <i class="fa fa-4x fa-file-code-o text-primary sr-icons"></i>
                    <h3 class="h3-text">Core PHP</h3>
                    <p class="text-muted">I build website by "core" Php, JavaScript as needed without any platform.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box">
                    <i class="fa fa-4x fa-object-group text-primary sr-icons"></i>
                    <h3 class="h3-text">Front-end</h3>
                    <p class="text-muted">I use Wordpress for CMS and Phalcon and Laravel for MVC platform for back-end.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box">
                    <i class="fa fa-4x fa-object-ungroup text-primary sr-icons"></i>
                    <h3 class="h3-text">Back-end</h3>
                    <p class="text-muted">For front-end development, mainly I use JQuery. I have also little experience in Angular-JS</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box">
                    <i class="fa fa-4x fa-tablet text-primary sr-icons"></i>
                    <h3 class="h3-text">Responsive Design</h3>
                    <p class="text-muted">I create responsive (mobile friendly) website using Bootstrap and Media Query.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="no-padding" id="portfolio">
    <div class="container-fluid">
        <div class="row no-gutter popup-gallery">
            <div class="col-lg-4 col-sm-6">
                <a href="img/portfolio/fullsize/site1.jpg" class="portfolio-box">
                    <img src="img/portfolio/thumbnails/site1.jpg" class="img-responsive" alt="Portfolio site 1">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Single Page
                            </div>
                            <div class="project-name">
                                Single page static portfolio
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="img/portfolio/fullsize/site2.JPG" class="portfolio-box">
                    <img src="img/portfolio/thumbnails/site2.JPG" class="img-responsive" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Multi Page
                            </div>
                            <div class="project-name">
                                Multi page music site example
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="img/portfolio/fullsize/site3.jpg" class="portfolio-box">
                    <img src="img/portfolio/thumbnails/site3.jpg" class="img-responsive" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Wordpress
                            </div>
                            <div class="project-name">
                                This is example of a Wordpress child theme
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!--<div class="col-lg-4 col-sm-6">
                <a href="img/portfolio/fullsize/4.jpg" class="portfolio-box">
                    <img src="img/portfolio/thumbnails/4.jpg" class="img-responsive" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Category
                            </div>
                            <div class="project-name">
                                Project Name
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="img/portfolio/fullsize/5.jpg" class="portfolio-box">
                    <img src="img/portfolio/thumbnails/5.jpg" class="img-responsive" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Category
                            </div>
                            <div class="project-name">
                                Project Name
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="img/portfolio/fullsize/6.jpg" class="portfolio-box">
                    <img src="img/portfolio/thumbnails/6.jpg" class="img-responsive" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Category
                            </div>
                            <div class="project-name">
                                Project Name
                            </div>
                        </div>
                    </div>
                </a>
            </div>-->
        </div>
    </div>
</section>
<!--Footer block-->
<aside id="contact" class="bg-dark">
    <div class="container text-center">
        <div class="call-to-action">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading-footer">Find me in Google map</h2>
                    <hr class="primary">
                    <!-- Map -->
                    <div class="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3680.8787810256904!2d88.46290341461456!3d22.695556034166582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39f89f4895555555%3A0xe436f2215dc9c45d!2sAppliPlus+India!5e0!3m2!1sen!2sin!4v1481035694951"
                                width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <!-- /Map -->
                    <br>
                    <h2 class="section-heading-footer">Leave me an Email</h2>
                    <hr class="primary">
                </div>

                <form>
                    <div class="col-lg-4 col-lg-offset-2 text-center sr-contact">
                        <div class="form-group">
                            <input type="text" class="form-control" id="NameInputEmail1" placeholder="Your Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="subjectEmail1" placeholder="Subject">
                        </div>
                    </div>
                    <div class="col-lg-4 text-center">
                        <div class="form-group">
                            <textarea class="form-control sr-contact" rows="6"
                                      placeholder="Enter your text here"></textarea>
                        </div>
                    </div>

                    <div class="col-lg-8 col-lg-offset-2 text-center">
                        <button type="submit" class="btn btn-default btn-xl sr-contact">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</aside>

<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/scrollReveal.js/3.3.2/scrollreveal.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>

<!-- Theme JavaScript -->
<script src="js/creative.js"></script>

</body>

</html>
